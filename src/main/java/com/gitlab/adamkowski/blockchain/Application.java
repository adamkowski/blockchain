package com.gitlab.adamkowski.blockchain;

import com.gitlab.adamkowski.blockchain.blockchain.impl.SimpleBlockchain;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Application {

    public static final String BLOCKCHAIN_FILE_NAME = "blockchain";

    public static void main(String[] args) {
        SimpleBlockchain blockchain = initializeBlockchain();
        IntStream.rangeClosed(1, 5)
                .forEach(i -> blockchain.generateBlock());
        blockchain.getBlocks().stream()
                .skip(Math.max(0, blockchain.getBlocks().size() - 5))
                .forEach(System.out::println);
        serializeBlockchain(blockchain);
    }

    private static SimpleBlockchain initializeBlockchain() {
        System.out.print("Enter how many zeros the hash must start with: ");
        final int leadingZeros = new Scanner(System.in).nextInt();
        System.out.println();

        if (Files.exists(Paths.get(BLOCKCHAIN_FILE_NAME))) {
            try (FileInputStream fis = new FileInputStream(BLOCKCHAIN_FILE_NAME);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                SimpleBlockchain blockchain = (SimpleBlockchain) ois.readObject();
                blockchain.setLeadingZeros(leadingZeros);
                return blockchain;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new SimpleBlockchain(leadingZeros);
    }

    private static void serializeBlockchain(SimpleBlockchain blockchain) {
        try (FileOutputStream fos = new FileOutputStream(BLOCKCHAIN_FILE_NAME);
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             ObjectOutputStream outputStream = new ObjectOutputStream(bos)) {
            outputStream.writeObject(blockchain);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
