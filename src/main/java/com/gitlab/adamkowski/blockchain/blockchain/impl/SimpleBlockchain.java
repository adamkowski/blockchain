package com.gitlab.adamkowski.blockchain.blockchain.impl;

import com.gitlab.adamkowski.blockchain.blockchain.api.Blockchain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class SimpleBlockchain implements Blockchain, Serializable {
    private static final long serialVersionUID = 1L;
    private static final String FIRST_BLOCK_PREVIOUS_HASH = "0";

    private final List<Block> blocks = new ArrayList<>();
    private int leadingZeros;

    public SimpleBlockchain(int leadingZeros) {
        this.leadingZeros = leadingZeros;
    }

    @Override
    public void generateBlock() {
        String previousHash = getPreviousBlockHash();
        int nextId = blocks.size() + 1;
        blocks.add(new Block(previousHash, nextId, leadingZeros));
    }

    @Override
    public boolean validate() {
        if (blocks.isEmpty()) {
            return true;
        }
        if (!Objects.equals(blocks.get(0).getPreviousBlockHash(), FIRST_BLOCK_PREVIOUS_HASH)) {
            return false;
        }
        if (!Objects.equals(collectCurrentHashes(), collectPreviousHashes())) {
            return false;
        }
        return blocks.stream()
                .map(Block::getHash)
                .anyMatch(hash -> !hash.startsWith("0".repeat(leadingZeros)));
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setLeadingZeros(int leadingZeros) {
        this.leadingZeros = leadingZeros;
    }

    private String getPreviousBlockHash() {
        if (blocks.isEmpty()) {
            return FIRST_BLOCK_PREVIOUS_HASH;
        }
        Block lastBlock = blocks.get(blocks.size() - 1);
        return lastBlock.getHash();
    }

    private List<String> collectCurrentHashes() {
        return blocks.stream()
                .limit(blocks.size() - 1)
                .map(Block::getHash)
                .collect(toList());
    }

    private List<String> collectPreviousHashes() {
        return blocks.stream()
                .skip(1L)
                .map(Block::getPreviousBlockHash)
                .collect(toList());
    }
}
