package com.gitlab.adamkowski.blockchain.blockchain.impl;

import com.gitlab.adamkowski.blockchain.blockchain.util.StringUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;

public class Block implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Random random = new Random();

    private final String previousBlockHash;
    private final String hash;
    private final long timeStamp = new Date().getTime();
    private final long id;
    private final int leadingZeros;
    private final long generationTimeInSeconds;
    private int magicNumber = random.nextInt();

    public Block(String previousBlockHash, long id, int leadingZeros) {
        this.previousBlockHash = previousBlockHash;
        this.id = id;
        this.leadingZeros = leadingZeros;
        hash = hash();
        generationTimeInSeconds = (new Date().getTime() - timeStamp) / 1000;
    }

    public String getPreviousBlockHash() {
        return previousBlockHash;
    }

    public String getHash() {
        return hash;
    }

    public long getId() {
        return id;
    }

    private String hash() {
        final String blockContent = previousBlockHash + timeStamp + id;
        String hash = StringUtil.applySha256(blockContent + magicNumber);
        while (!hash.startsWith("0".repeat(leadingZeros))) {
            magicNumber = random.nextInt();
            hash = StringUtil.applySha256(blockContent + magicNumber);
        }
        return hash;
    }

    @Override
    public String toString() {
        return "Block:" + System.lineSeparator() +
                "Id: " + id + System.lineSeparator() +
                "Timestamp: " + timeStamp + System.lineSeparator() +
                "Magic number: " + magicNumber + System.lineSeparator() +
                "Hash of the previous block: " + System.lineSeparator() + previousBlockHash + System.lineSeparator() +
                "Hash of the block: " + System.lineSeparator() + hash + System.lineSeparator() +
                String.format("Block was generating for %d seconds%n", generationTimeInSeconds);
    }
}
