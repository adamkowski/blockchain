package com.gitlab.adamkowski.blockchain.blockchain.api;

public interface Blockchain {
    boolean validate();

    void generateBlock();
}
