package com.gitlab.adamkowski.blockchain.blockchain.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimpleBlockchainTest {

    private final SimpleBlockchain blockchain = new SimpleBlockchain(2);

    @Test
    @DisplayName("Empty blockchain should be valid")
    void emptyBlockchainShouldBeValid() {
        assertTrue(blockchain.validate());
    }

    @Test
    @DisplayName("In the first block, its hash of the previous block should be zero")
    void inTheFirstBlockItsHashOfThePreviousBlockShouldBeZero() {
        blockchain.generateBlock();
        List<Block> blocks = blockchain.getBlocks();
        String previousBlockHash = blocks.get(0).getPreviousBlockHash();
        assertEquals("0", previousBlockHash);
    }

    @Test
    @DisplayName("First block id should be equal one")
    void firstBlockIdShouldBeEqualOne() {
        blockchain.generateBlock();
        Block firstBlock = blockchain.getBlocks().get(0);
        assertEquals(1L, firstBlock.getId());
    }

    @Test
    @DisplayName("Blockchain allows to change hash prefix")
    void blockchainAllowsToChangeHashPrefix() {
        blockchain.generateBlock();
        assertTrue(blockchain.getBlocks().get(0).getHash().startsWith("00"));

        blockchain.setLeadingZeros(5);
        blockchain.generateBlock();
        assertTrue(blockchain.getBlocks().get(1).getHash().startsWith("00000"));
    }
}
