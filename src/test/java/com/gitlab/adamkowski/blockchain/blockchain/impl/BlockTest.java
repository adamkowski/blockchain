package com.gitlab.adamkowski.blockchain.blockchain.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BlockTest {

    @Test
    @DisplayName("The block hash should start with a specified number of zeros")
    void theBlockHashShouldStartWithASpecifiedNumberOfZeros() {
        Block block = new Block("0", 1L, 5);
        assertTrue(block.getHash().startsWith("0".repeat(5)));
    }

    @Test
    @DisplayName("The block hash cannot be empty")
    void theBlockHashCannotBeEmpty() {
        Block block = new Block("0", 1L, 0);
        assertNotEquals("", block.getHash());
    }
}
