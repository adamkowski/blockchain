# Blockchain

Inspired by a JetBrains Academy project

## About the project

A description of the original project is available at https://hyperskill.org/projects/50. It is classified as the
highest degree of difficulty available on the JB Academy platform. This implementation extends this task. Instead of
just the source code, I created a complete project managed by Gradle. To improve the development, I have prepared my own
unit tests to ensure that the code complies with the requirements.

## Implementation

Project implementation consists of six stages, each of them has a description and requires the creation of code.
Compatibility of the created code is verified by original tests prepared by the creators and run on their platform.

### Stage 1/6: Blockchain essentials

Basic blockchain that defines the block creation operation and validation. Each subsequent block contains the hash of
the previous block.

### Stage 2/6: A proof of work concept

Basic blockchain protection against block modification in the form of required computational work by setting the hash
prefix. Serializing blockchain to a file and resuming work after restarting the application.
